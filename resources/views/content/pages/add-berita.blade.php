@extends('layouts/contentLayoutMaster')

@section('title', 'Account')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-pills mb-2">
                <!-- Account -->

                <!-- security -->

                <!-- billing and plans -->

                <!-- notification -->

                <!-- connection -->


                <!-- profile -->
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Input Berita</h4>
                    </div>
                    <div class="card-body py-2 my-25">
                        <!-- header section -->

                        <!-- upload and reset button -->

                        <!--/ upload and reset button -->

                        <!--/ header section -->

                        <!-- form -->
                        <form class="validate-form mt-2 pt-50">
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Judul</label>
                                    <input type="text" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan Judul Disini" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountLastName">Sub Judul</label>
                                    <input type="text" class="form-control" id="accountLastName" name="lastName"
                                        placeholder="Sub Judul" value="" data-msg="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="">Status</label>
                                    <select id="country" class="select2 form-select">
                                        <option value="">Pilih Status</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="Canada">Canada</option>
                                        <option value="China">China</option>
                                        <option value="France">France</option>
                                        <option value="Germany">Germany</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Korea">Korea, Republic of</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Russia">Russian Federation</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="">Kanal/Tipe</label>
                                    <select id="country" class="select2 form-select">
                                        <option value="">Pilih Kanal/Tipe</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="Canada">Canada</option>
                                        <option value="China">China</option>
                                        <option value="France">France</option>
                                        <option value="Germany">Germany</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Korea">Korea, Republic of</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Russia">Russian Federation</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="">Kategori</label>
                                    <select id="country" class="select2 form-select">
                                        <option value="">Pilih Kategori</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="Canada">Canada</option>
                                        <option value="China">China</option>
                                        <option value="France">France</option>
                                        <option value="Germany">Germany</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Korea">Korea, Republic of</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Russia">Russian Federation</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <div class="card">
                                        <div class="demo-inline-spacing">
                                            <label class="form-label" for="kategori">Add Kategori</label>
                                            <a class="btn btn-primary" id="html-alert"><i class="avatar-icon"
                                                    data-feather="plus"></i></a>
                                            <label class="form-label" for="kategori">Hapus Kategori</label>
                                            <button type="reset" class="btn btn-danger" id=""><i class="avatar-icon"
                                                    data-feather="trash-2"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountAddress">Tanggal Publish</label>
                                    <input type="date" class="form-control" id="accountAddress" name="address"
                                        placeholder="Your Address" />
                                </div>

                                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
                                                                integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
                                                                crossorigin="anonymous"></script>
                                <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css"
                                    rel="stylesheet">
                                <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
                                <div id="summernote"></div>
                                <script>
                                    $('#summernote').summernote({
                                        placeholder: '',
                                        tabsize: 1,
                                        height: 100,
                                        toolbar: [
                                            ['style', ['style']],
                                            ['font', ['bold', 'underline', 'clear']],
                                            ['color', ['color']],
                                            ['para', ['ul', 'ol', 'paragraph']],
                                            ['table', ['table']],
                                            ['insert', ['link', 'picture', 'video']],
                                            ['view', ['fullscreen', 'codeview', 'help']]
                                        ]
                                    });
                                </script>
                                <p>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountLastName">Upload File</label>
                                    <input type="file" class="form-control" id="accountLastName" name="lastName"
                                        placeholder="Upload File Disini" value="" data-msg="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountLastName">Foto</label>
                                    <input type="file" class="form-control" id="accountLastName" name="lastName"
                                        placeholder="Upload Foto Disini" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountLastName">URL</label>
                                    <input type="url" class="form-control" id="accountLastName" name="lastName"
                                        placeholder="Masukkan URL Link Disini" value="" data-msg="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountLastName">Sumber Informasi</label>
                                    <input type="text" class="form-control" id="accountLastName" name="lastName"
                                        placeholder="Masukkan Sumber Informasi Disini" value="" data-msg="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountLastName">Tags</label>
                                    <input type="text" class="form-control" id="accountLastName" name="lastName"
                                        placeholder="" value="" data-msg="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="">Parent</label>
                                    <select id="country" class="select2 form-select">
                                        <option value="">Pilih Parent</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="Canada">Canada</option>
                                        <option value="China">China</option>
                                        <option value="France">France</option>
                                        <option value="Germany">Germany</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Korea">Korea, Republic of</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Russia">Russian Federation</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary mt-1 me-1">Save changes</button>
                                    <button type="reset" class="btn btn-outline-secondary mt-1">Discard</button>
                                </div>
                            </div>
                        </form>
                        <!--/ form -->
                    </div>
                </div>

                <!-- deactivate account  -->
                <form id="formAccountDeactivation" class="validate-form" onsubmit="true">
                    <!--/ profile -->
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/pages/page-account-settings-account.js')) }}"></script>
    <script src="{{ asset('js/scripts/extensions/kategori-berita.js') }}"></script>
@endsection
