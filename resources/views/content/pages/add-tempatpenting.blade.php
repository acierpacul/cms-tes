@extends('layouts/contentLayoutMaster')

@section('title', 'Add Tempat Penting')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/maps/leaflet.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/maps/map-leaflet.css')) }}">
    <!-- summernote text editor -->

@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-pills mb-2">
                <!-- Account -->

                <!-- security -->

                <!-- billing and plans -->

                <!-- notification -->

                <!-- connection -->


                <!-- profile -->
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Input Tempat Penting</h4>
                    </div>
                    <div class="card-body py-2 my-25">


                        <!-- header section -->

                        <!-- upload and reset button -->

                        <!--/ upload and reset button -->

                        <!--/ header section -->

                        <!-- form -->
                        <form class="validate-form mt-2 pt-50">
                            <div class="row">
                                <center>
                                    <div class="d-block">
                                        <a href="#" class="mr-0">
                                            <img src="{{ asset('images/portrait/small/avatar-s-11.jpg') }}"
                                                id="account-upload-img" class="uploadedAvatar rounded me-50"
                                                alt="profile image" height="100" width="100" />
                                        </a>
                                        <div class="d-block mt-75">
                                            <div>
                                                <label for="account-upload"
                                                    class="btn btn-sm btn-primary mb-75 ">Upload</label>
                                                <input type="file" id="account-upload" hidden accept="image/*" />
                                                <p class="mb-0">Allowed file types: png, jpg, jpeg.</p>
                                            </div>
                                        </div>
                                    </div>
                                </center>
                                <p></p>
                                <p></p>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="namaTempat">Nama Tempat</label>
                                    <input type="text" class="form-control" id="namaTempat" name="namaTempat"
                                        placeholder="Nama Tempat"/>
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="penanggungJawab">Penanggung Jawab</label>
                                    <input type="text" class="form-control" id="penanggungJawab" name="penanggungJawab"
                                        placeholder="Penanggung Jawab" data-msg="Penanggung Jawab" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="country">Kategori</label>
                                    <select id="country" class="select2 form-select">
                                        <option value="">Select Kategori</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Belarus">Belarus</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <div class="card">
                                        <div class="demo-inline-spacing">
                                            <label class="form-label" for="kategori">Add Kategori</label>
                                            <button class="btn btn-primary" id="html-alert"><i class="avatar-icon"
                                                    data-feather="plus"></i></button>
                                            <label class="form-label" for="kategori">Hapus Kategori</label>
                                            <button type="reset" class="btn btn-danger" id=""><i class="avatar-icon"
                                                    data-feather="trash-2"></i></button>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountOrganization">Email</label>
                                    <input type="email" class="form-control" id="accountOrganization" name="organization"
                                        placeholder="Masukkan Email Disini" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountPhoneNumber">Nomor Telepon</label>
                                    <input type="text" class="form-control account-number-mask" id="accountPhoneNumber"
                                        name="phoneNumber" placeholder="Masukkan Nomor Telepon" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountAddress">Alamat</label>
                                    <input type="text" class="form-control" id="accountAddress" name="address"
                                        placeholder="Masukkan Alamat Disini" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountState">Website</label>
                                    <input type="text" class="form-control" id="accountState" name="state"
                                        placeholder="Masukkan URL Website Disini" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountZipCode">Facebook</label>
                                    <input type="text" class="form-control account-zip-code" id="accountZipCode"
                                        name="zipCode" placeholder="Masukkan Username Facebook" maxlength="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountZipCode">Instagram</label>
                                    <input type="text" class="form-control account-zip-code" id="accountZipCode"
                                        name="zipCode" placeholder="@username" maxlength="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountZipCode">Koordinat Bujur</label>
                                    <input type="text" class="form-control account-zip-code" id="accountZipCode"
                                        name="zipCode" placeholder="Masukkan Koordinat Bujur" maxlength="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountZipCode">Koordinat Lintang</label>
                                    <input type="text" class="form-control account-zip-code" id="accountZipCode"
                                        name="zipCode" placeholder="Masukkan Koordinat Lintang" maxlength="" />
                                </div>

                                <div class="col-12">
                                    <div class="card mb-4">
                                        <div class="card-header">
                                            <h4 class="card-title">Map</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="leaflet-map" id="drag-map"></div>
                                        </div>
                                    </div>
                                </div>
                                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
                                                                integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
                                                                crossorigin="anonymous"></script>
                                <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css"
                                    rel="stylesheet">
                                <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
                                <div id="summernote"></div>
                                <script>
                                    $('#summernote').summernote({
                                        placeholder: '',
                                        tabsize: 1,
                                        height: 100,
                                        toolbar: [
                                            ['style', ['style']],
                                            ['font', ['bold', 'underline', 'clear']],
                                            ['color', ['color']],
                                            ['para', ['ul', 'ol', 'paragraph']],
                                            ['table', ['table']],
                                            ['insert', ['link', 'picture', 'video']],
                                            ['view', ['fullscreen', 'codeview', 'help']]
                                        ]
                                    });
                                </script>
                                <br>
                                <p>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary mt-1 me-1">Save changes</button>
                                    <button type="reset" class="btn btn-outline-secondary mt-1">Discard</button>
                                </div>
                            </div>
                        </form>
                        <!--/ form -->
                    </div>
                </div>

                <!-- deactivate account  -->
                <form id="formAccountDeactivation" class="validate-form" onsubmit="true">

        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/maps/leaflet.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/maps/map-leaflet.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/page-account-settings-account.js')) }}"></script>
    <script src="{{ asset('js/scripts/extensions/kategori-tempatpenting.js') }}"></script>
@endsection
