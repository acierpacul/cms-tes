@extends('layouts/contentLayoutMaster')

@section('title', 'Add Site')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-pills mb-2">
                <!-- Account -->

                <!-- security -->

                <!-- billing and plans -->

                <!-- notification -->

                <!-- connection -->


                <!-- profile -->
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Input Site</h4>
                    </div>
                    <div class="card-body py-2 my-25">
                        <!-- header section -->

                        <!-- upload and reset button -->

                        <!--/ upload and reset button -->

                        <!--/ header section -->

                        <!-- form -->
                        <form class="validate-form mt-2 pt-50">
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Kode</label>
                                    <input type="text" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan Kode" value="" />
                                </div>
                            
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountAddress">Nama</label>
                                    <input type="text" class="form-control" id="accountAddress" name="address"
                                        placeholder="Nama Situs" />
                                </div>

                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountAddress">URL/Domain</label>
                                    <input type="link" class="form-control" id="accountAddress" name="address"
                                        placeholder="Masukkan Link" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="country">Pilih Parent</label>
                                    <select id="country" class="select2 form-select">
                                        <option value="">Select Parent</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Belarus">Belarus</option>
                                    </select>
                                </div>
                                    <p>
                                    <label class="form-label" for="accountAddress">Deskripsi</label>
                                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
                                                                integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
                                                                crossorigin="anonymous"></script>
                                <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css"
                                    rel="stylesheet">
                                <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
                                <div id="summernote"></div>
                                <script>
                                    $('#summernote').summernote({
                                        placeholder: '',
                                        tabsize: 1,
                                        height: 100,
                                        toolbar: [
                                            ['style', ['style']],
                                            ['font', ['bold', 'underline', 'clear']],
                                            ['color', ['color']],
                                            ['para', ['ul', 'ol', 'paragraph']],
                                            ['table', ['table']],
                                            ['insert', ['link', 'picture', 'video']],
                                            ['view', ['fullscreen', 'codeview', 'help']]
                                        ]
                                    });
                                </script>
                                <p>
                              
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary mt-1 me-1">Save changes</button>
                                    <button type="reset" class="btn btn-outline-secondary mt-1">Discard</button>
                                </div>
                            </div>
                        </form>
                        <!--/ form -->
                    </div>
                </div>

                <!-- deactivate account  -->
                <form id="formAccountDeactivation" class="validate-form" onsubmit="true">
                    <!--/ profile -->
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/pages/page-account-settings-account.js')) }}"></script>
@endsection
