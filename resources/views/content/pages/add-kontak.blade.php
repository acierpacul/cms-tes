@extends('layouts/contentLayoutMaster')

@section('title', 'Add Kontak')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-pills mb-2">
                <!-- Account -->

                <!-- security -->

                <!-- billing and plans -->

                <!-- notification -->

                <!-- connection -->


                <!-- profile -->
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Input Kontak</h4>
                    </div>
                    <div class="card-body py-2 my-25">
                        <!-- header section -->

                        <!-- upload and reset button -->

                        <!--/ upload and reset button -->

                        <!--/ header section -->

                        <!-- form -->
                        <form class="validate-form mt-2 pt-50">
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan Nama Lengkap" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Nama Panggilan</label>
                                    <input type="text" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan NickName" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">No. Telp</label>
                                    <input type="number" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan No. Telp" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Email</label>
                                    <input type="email" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan Email" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">No. Handphone</label>
                                    <input type="number" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan No. Handphone" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="">Organisasi</label>
                                    <select id="country" class="select2 form-select">
                                        <option value="">Pilih Organisasi</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="">Jabatan</label>
                                    <select id="country" class="select2 form-select">
                                        <option value="">Pilih Jabatan</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Foto</label>
                                    <input type="file" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan Foto" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Facebook</label>
                                    <input type="text" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan Username FB" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Instagram</label>
                                    <input type="text" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan Username IG" value="@" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Alamat</label>
                                    <input type="text-area" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="Masukkan Alamat" value="" />
                                </div>
                                <div class="col-12 col-sm-6 mb-1">
                                    <label class="form-label" for="accountFirstName">Deskripsi</label>
                                    <input type="text-area" class="form-control" id="accountFirstName" name="firstName"
                                        placeholder="" value="" />
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary mt-1 me-1">Save changes</button>
                                    <button type="reset" class="btn btn-outline-secondary mt-1">Discard</button>
                                </div>
                            </div>
                        </form>
                        <!--/ form -->
                    </div>
                </div>

                <!-- deactivate account  -->
                <form id="formAccountDeactivation" class="validate-form" onsubmit="true">
                    <!--/ profile -->
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/pages/page-account-settings-account.js')) }}"></script>
@endsection
