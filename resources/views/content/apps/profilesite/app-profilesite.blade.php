@extends('layouts/contentLayoutMaster')

@section('title', 'Profile Site')

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')
<section class="app-user-view-security">
  <div class="row">
    <!-- User Sidebar -->
    <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
      <!-- User Card -->
      <div class="card">
        <div class="card-body">
          <div class="user-avatar-section">
            <div class="d-flex align-items-center flex-column">
              <img
                class="img-fluid rounded mt-3 mb-2"
                src="{{('http://cms.smartcity.co.id/assets/images/logo2.jpg')}}"
                height="110"
                width="110"
                alt="User avatar"
              />
              <div class="user-info text-center">
                <h4>Portal Smart City</h4>
                <span class="badge bg-light-secondary">Admin</span>
              </div>
            </div>
          </div>
          <div class="d-flex justify-content-around my-2 pt-75">
            <div class="d-flex align-items-start me-2">
              <span class="badge bg-light-primary p-75 rounded">
                <i data-feather="phone" class="font-medium-2"></i>
              </span>
              <div class="ms-75">
                <h4 class="mb-0">08111232222</h4>
                <small><a>Whatsapp</a></small>
              </div>
            </div>
          </div>
          <h4 class="fw-bolder border-bottom pb-50 mb-1">Details</h4>
          <div class="info-container">
            <ul class="list-unstyled">
              <li class="mb-75">
                <span class="fw-bolder me-25">Alamat :</span>
                <span><a href="https://www.google.com/maps/place/Balaikota+Depok/@-6.3935467,106.8189761,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69ebe306c3c2b1:0x46d80d4d5f28d709!8m2!3d-6.393552!4d106.8211701">
                    Jl. Margonda Raya No.54, Depok, Kec. Pancoran Mas, Kota Depok, Jawa Barat 16431</a></span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Email :</span>
                <span>portal@depok.go.id</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Facebook :</span>
                <span><a href="https://www.facebook.com/pemkotdepok"> Visit Facebook </a></span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Instagram :</span>
                <span><a href="http://devsc.id/cms_smartcity/Home/profilsite">Visit Instagram</a></span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Youtube :</span>
                <span><a href="https://www.youtube.com/user/kominfodepok/videos">Visit Youtube</a></span>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /User Card -->
      <!-- Plan Card -->
     
      <!-- /Plan Card -->
    </div>
    <!--/ User Sidebar -->

    <!-- User Content -->
    <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
      <!-- User Pills -->
     
      <!--/ User Pills -->

      <!-- Change Password -->
      <div class="card">
        <div class="card-body">
          <form id="formChangePassword" method="POST" onsubmit="return false">
            <div class="row">
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">Nama Site</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="text"
                    id=""
                    name=""
                    placeholder=""
                    value="Portal Smart City"
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">Status Akreditasi</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="text"
                    id=""
                    name=""
                    placeholder=""
                    value=""
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">Jadwal Buka</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="text"
                    id=""
                    name=""
                    placeholder=""
                    value=""
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">Kode Surat Resmi</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="text"
                    id=""
                    name=""
                    placeholder=""
                    value=""
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">Deskripsi</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="text"
                    id=""
                    name=""
                    placeholder=""
                    value="Merupakan sebuah media yang memberikan kemudahan 
                    layanan dan informasi terkait Smart City kepada warga Kota Depok, 
                    dengan mengelola semua potensi sumber daya secara efektif dan efisien dengan tujuan untuk meningkatkan pelayanan publik, dan kesejahteraan warga."
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">Alamat</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="text"
                    id=""
                    name=""
                    placeholder=""
                    value="Jl. Margonda Raya No.54, Depok, Kec. Pancoran Mas, Kota Depok, Jawa Barat 16431"
                  />
                </div>
              </div>
              <div class="col-12 col-sm-6 mb-1">
                <label class="form-label" for="country">Kecamatan</label>
                    <select id="country" class="select2 form-select">
                        <option value="">Beji</option>
                        <option value="Australia">Australia</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Belarus">Belarus</option>
                    </select>
                </div>
                <div class="col-12 col-sm-6 mb-1">
                <label class="form-label" for="country">Kelurahan</label>
                    <select id="country" class="select2 form-select">
                        <option value="">Beji Timur</option>
                        <option value="Australia">Australia</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Belarus">Belarus</option>
                    </select>
                </div>
                <div class="col-12 col-sm-6 mb-1">
                <label class="form-label" for="country">PIC</label>
                    <select id="country" class="select2 form-select">
                        <option value="">Pilih PIC</option>
                        <option value="Australia">Australia</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Belarus">Belarus</option>
                    </select>
                </div>
                <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">URL Alamat Maps</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="url"
                    id=""
                    name=""
                    placeholder=""
                    value="https://www.google.com/maps/place/Balaikota+Depok/@-6.3935467,106.8189761,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69ebe306c3c2b1:0x46d80d4d5f28d709!8m2!3d-6.393552!4d106.8211701"
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">No. Telepon</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="number"
                    id=""
                    name=""
                    placeholder=""
                    value="08111232222"
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">Email</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="email"
                    id=""
                    name=""
                    placeholder=""
                    value="portal@depok.go.id"
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">URL Whatsapp</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="url"
                    id=""
                    name=""
                    placeholder=""
                    value=""
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">URL Facebook</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="url"
                    id=""
                    name=""
                    placeholder=""
                    value="https://www.facebook.com/pemkotdepok"
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">URL Twitter</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="url"
                    id=""
                    name=""
                    placeholder=""
                    value=""
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">URL Instagram</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="url"
                    id=""
                    name=""
                    placeholder=""
                    value="http://devsc.id/cms_smartcity/Home/profilsite"
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">URL Youtube</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="url"
                    id=""
                    name=""
                    placeholder=""
                    value="https://www.youtube.com/user/kominfodepok/videos"
                  />
                </div>
              </div>
              <div class="mb-2 col-md-6 form-password-toggle">
                <label class="form-label" for="newPassword">Visi</label>
                <div class="input-group">
                  <input
                    class="form-control"
                    type="text"
                    id=""
                    name=""
                    placeholder=""
                    value=""
                  />
                </div>
              </div>
              <label class="form-label" for="newPassword">Misi</label>
              <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
                                                                integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
                                                                crossorigin="anonymous"></script>
                                <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css"
                                    rel="stylesheet">
                                <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
                                <div id="summernote"></div>
                                <script>
                                    $('#summernote').summernote({
                                        placeholder: '',
                                        tabsize: 1,
                                        height: 100,
                                        toolbar: [
                                            ['style', ['style']],
                                            ['font', ['bold', 'underline', 'clear']],
                                            ['color', ['color']],
                                            ['para', ['ul', 'ol', 'paragraph']],
                                            ['table', ['table']],
                                            ['insert', ['link', 'picture', 'video']],
                                            ['view', ['fullscreen', 'codeview', 'help']]
                                        ]
                                    });
                                </script>
                                <p>
                                    <p>
              <div>
                <button type="submit" class="btn btn-primary me-2">Change Password</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--/ Change Password -->

      <!-- Two-steps verification -->
     
      <!--/ Two-steps verification -->

      <!-- recent device -->
     
      <!-- / recent device -->
    </div>
    <!--/ User Content -->
  </div>
</section>


@include('content/_partials/_modals/modal-edit-user')
@include('content/_partials/_modals/modal-upgrade-plan')
@include('content/_partials/_modals/modal-two-factor-auth')
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/modal-two-factor-auth.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/modal-edit-user.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/app-user-view-security.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/app-user-view.js')) }}"></script>
@endsection
