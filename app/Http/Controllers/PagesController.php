<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{

    // Account Settings account
    public function add_organisasi()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/organisasi", 'name' => "Organisasi"], ['name' => "Add Organisasi"]];
        return view('/content/pages/add-organisasi', ['breadcrumbs' => $breadcrumbs]);
    }
    // Account Settings account
    public function add_jabatan()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/jabatan", 'name' => "Jabatan"], ['name' => "Add Jabatan"]];
        return view('/content/pages/add-jabatan', ['breadcrumbs' => $breadcrumbs]);
    }
    // Account Settings account
    public function add_tempatpenting()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/tempatpenting", 'name' => "Tempat Penting"], ['name' => "Add Tempat Penting"]];
        return view('/content/pages/add-tempatpenting', ['breadcrumbs' => $breadcrumbs]);
    }
    // Add Site
    public function add_daftarsite()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/daftarsite", 'name' => "Site"], ['name' => "Add Site"]];
        return view('/content/pages/add-daftarsite', ['breadcrumbs' => $breadcrumbs]);
    }
    // Add Kontak
    public function add_kontak()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/kontak", 'name' => "Kontak"], ['name' => "Add Kontak"]];
        return view('/content/pages/add-kontak', ['breadcrumbs' => $breadcrumbs]);
    }
    // Add agenda
    public function add_agenda()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/agenda", 'name' => "Agenda"], ['name' => "Add Agenda"]];
        return view('/content/pages/add-agenda', ['breadcrumbs' => $breadcrumbs]);
    }
    // Add galeri
    public function add_galeri()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/galeri", 'name' => "Galeri"], ['name' => "Add Galeri"]];
        return view('/content/pages/add-galeri', ['breadcrumbs' => $breadcrumbs]);
    }
    // Add slider
    public function add_slider()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/slider", 'name' => "Slider"], ['name' => "Add Slider"]];
        return view('/content/pages/add-slider', ['breadcrumbs' => $breadcrumbs]);
    }
    // Add menu link
    public function add_menulink()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/menulink", 'name' => "Menu & Link"], ['name' => "Add Menu  & Link"]];
        return view('/content/pages/add-menulink', ['breadcrumbs' => $breadcrumbs]);
    }





    // Add Berita
    public function add_berita()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/berita", 'name' => "Berita"], ['name' => "Add Berita"]];
        return view('/content/pages/add-berita', ['breadcrumbs' => $breadcrumbs]);
    }

    // Add Pengumuman
    public function add_pengumuman()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/pengumuman", 'name' => "Pengumuman"], ['name' => "Add Pengumuman"]];
        return view('/content/pages/add-pengumuman', ['breadcrumbs' => $breadcrumbs]);
    }


    // Add knowladge
    public function add_knowledge()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/knowledge", 'name' => "Knowledge"], ['name' => "Add Knowledge"]];
        return view('/content/pages/add-knowledge', ['breadcrumbs' => $breadcrumbs]);
    }

    // Add product layanan
    public function add_productlayanan()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/productlayanan", 'name' => "Product Layanan"], ['name' => "Add Product Layanan"]];
        return view('/content/pages/add-productlayanan', ['breadcrumbs' => $breadcrumbs]);
    }

    // Add Profil layanan
    public function add_profil()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/profil", 'name' => "Profil"], ['name' => "Add Profil"]];
        return view('/content/pages/add-profil', ['breadcrumbs' => $breadcrumbs]);
    }

    // Add Lembaga layanan
    public function add_lembaga()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/lembaga", 'name' => "Lembaga"], ['name' => "Add Lembaga"]];
        return view('/content/pages/add-lembaga', ['breadcrumbs' => $breadcrumbs]);
    }

    // Add Lembaga layanan
    public function add_programunggulan()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/programunggulan", 'name' => "Program Unggulan"], ['name' => "Add Program Unggulan"]];
        return view('/content/pages/add-programunggulan', ['breadcrumbs' => $breadcrumbs]);
    }

    // Add Lembaga layanan
    public function add_strukturorganisasi()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/strukturorganisasi", 'name' => "Struktur Organisasi"], ['name' => "Add Struktur Organisasi"]];
        return view('/content/pages/add-strukturorganisasi', ['breadcrumbs' => $breadcrumbs]);
    }

    // Add Lembaga layanan
    public function add_jadwallayanan()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/app/jadwallayanan", 'name' => "JadwaL layanan"], ['name' => "Add JadwaL Layanan"]];
        return view('/content/pages/add-jadwallayanan', ['breadcrumbs' => $breadcrumbs]);
    }






    // Account Settings account
    public function account_settings_account()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Account Settings"], ['name' => "Account"]];
        return view('/content/pages/page-account-settings-account', ['breadcrumbs' => $breadcrumbs]);
    }

    // Account Settings security
    public function account_settings_security()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Account Settings"], ['name' => "Security"]];
        return view('/content/pages/page-account-settings-security', ['breadcrumbs' => $breadcrumbs]);
    }

    // Account Settings billing
    public function account_settings_billing()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Account Settings"], ['name' => "Billing & Plans"]];
        return view('/content/pages/page-account-settings-billing', ['breadcrumbs' => $breadcrumbs]);
    }

    // Account Settings notifications
    public function account_settings_notifications()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Account Settings"], ['name' => "Notifications"]];
        return view('/content/pages/page-account-settings-notifications', ['breadcrumbs' => $breadcrumbs]);
    }

    // Account Settings connections
    public function account_settings_connections()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Account Settings"], ['name' => "Connections"]];
        return view('/content/pages/page-account-settings-connections', ['breadcrumbs' => $breadcrumbs]);
    }

    // Profile
    public function profile()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['name' => "Profile"]];

        return view('/content/pages/page-profile', ['breadcrumbs' => $breadcrumbs]);
    }

    // FAQ
    public function faq()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['name' => "FAQ"]];
        return view('/content/pages/page-faq', ['breadcrumbs' => $breadcrumbs]);
    }

    // Knowledge Base
    public function knowledge_base()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['name' => "Knowledge Base"]];
        return view('/content/pages/page-knowledge-base', ['breadcrumbs' => $breadcrumbs]);
    }

    // Knowledge Base Category
    public function kb_category()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['link' => "/page/knowledge-base", 'name' => "Knowledge Base"], ['name' => "Category"]];
        return view('/content/pages/page-kb-category', ['breadcrumbs' => $breadcrumbs]);
    }

    // Knowledge Base Question
    public function kb_question()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['link' => "/page/knowledge-base", 'name' => "Knowledge Base"], ['link' => "/page/knowledge-base/category", 'name' => "Category"], ['name' => "Question"]];
        return view('/content/pages/page-kb-question', ['breadcrumbs' => $breadcrumbs]);
    }

    // pricing
    public function pricing()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/pages/page-pricing', ['pageConfigs' => $pageConfigs]);
    }

    // api key
    public function api_key()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/pages/page-api-key', ['pageConfigs' => $pageConfigs]);
    }

    // blog list
    public function blog_list()
    {
        $pageConfigs = ['contentLayout' => 'content-detached-right-sidebar', 'bodyClass' => 'content-detached-right-sidebar'];

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['link' => "javascript:void(0)", 'name' => "Blog"], ['name' => "List"]];

        return view('/content/pages/page-blog-list', ['breadcrumbs' => $breadcrumbs, 'pageConfigs' => $pageConfigs]);
    }

    // blog detail
    public function blog_detail()
    {
        $pageConfigs = ['contentLayout' => 'content-detached-right-sidebar', 'bodyClass' => 'content-detached-right-sidebar'];

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['link' => "javascript:void(0)", 'name' => "Blog"], ['name' => "Detail"]];

        return view('/content/pages/page-blog-detail', ['breadcrumbs' => $breadcrumbs, 'pageConfigs' => $pageConfigs]);
    }

    // blog edit
    public function blog_edit()
    {

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['link' => "javascript:void(0)", 'name' => "Blog"], ['name' => "Edit"]];

        return view('/content/pages/page-blog-edit', ['breadcrumbs' => $breadcrumbs]);
    }

    // modal examples
    public function modal_examples()
    {

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Modal Examples"]];

        return view('/content/pages/modal-examples', ['breadcrumbs' => $breadcrumbs]);
    }

    // license
    public function license()
    {

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['name' => "License"]];

        return view('/content/pages/page-license', ['breadcrumbs' => $breadcrumbs]);
    }
}
