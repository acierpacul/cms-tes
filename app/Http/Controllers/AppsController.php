<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppsController extends Controller
{
    // Profile Site App
    public function profilesiteApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'profilesite-application',
        ];

        return view('/content/apps/profilesite/app-profilesite', ['pageConfigs' => $pageConfigs]);
    }
    // Tempat Penting App
    public function tempatpentingApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'tempatpenting-application',
        ];

        return view('/content/apps/tempatpenting/app-tempatpenting', ['pageConfigs' => $pageConfigs]);
    }


    // Organisasi App
    public function organisasiApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'organisasi-application',
        ];

        return view('/content/apps/organisasi/app-organisasi', ['pageConfigs' => $pageConfigs]);
    }

    // jabatan App
    public function jabatanApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'jabatan-application',
        ];

        return view('/content/apps/jabatan/app-jabatan', ['pageConfigs' => $pageConfigs]);
    }

    // Site App
    public function daftarsiteApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'daftarsite-application',
        ];

        return view('/content/apps/daftarsite/app-daftarsite', ['pageConfigs' => $pageConfigs]);
    }
    // Kontak App
    public function kontakApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'kontak-application',
        ];

        return view('/content/apps/kontak/app-kontak', ['pageConfigs' => $pageConfigs]);
    }
    // Agenda App
    public function agendaApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'agenda-application',
        ];

        return view('/content/apps/agenda/app-agenda', ['pageConfigs' => $pageConfigs]);
    }
    // Galeri App
    public function galeriApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'galeri-application',
        ];

        return view('/content/apps/galeri/app-galeri', ['pageConfigs' => $pageConfigs]);
    }
    // Slider App
    public function sliderApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'slider-application',
        ];

        return view('/content/apps/slider/app-slider', ['pageConfigs' => $pageConfigs]);
    }
    // Menu Link App
    public function menulinkApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'menulink-application',
        ];

        return view('/content/apps/menulink/app-menulink', ['pageConfigs' => $pageConfigs]);
    }




    // Berita App
    public function beritaApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'berita-application',
        ];

        return view('/content/apps/berita/app-berita', ['pageConfigs' => $pageConfigs]);
    }

    // Pengumuman App
    public function pengumumanApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'pengumuman-application',
        ];

        return view('/content/apps/pengumuman/app-pengumuman', ['pageConfigs' => $pageConfigs]);
    }

    // knowledge App
    public function knowledgeApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'knowledge-application',
        ];

        return view('/content/apps/knowledge/app-knowledge', ['pageConfigs' => $pageConfigs]);
    }

    // Product Layanan App
    public function productlayananApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'productlayanan-application',
        ];

        return view('/content/apps/productlayanan/app-productlayanan', ['pageConfigs' => $pageConfigs]);
    }

    // Product Profil App
    public function profilApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'profil-application',
        ];

        return view('/content/apps/profil/app-profil', ['pageConfigs' => $pageConfigs]);
    }

    // Product Lembaga App
    public function lembagaApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'lembaga-application',
        ];

        return view('/content/apps/lembaga/app-lembaga', ['pageConfigs' => $pageConfigs]);
    }

    // Product Program Unggulan App
    public function programunggulanApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'programunggulan-application',
        ];

        return view('/content/apps/programunggulan/app-programunggulan', ['pageConfigs' => $pageConfigs]);
    }

    // Product Program Unggulan App
    public function strukturorganisasiApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'strukturorganisasi-application',
        ];

        return view('/content/apps/strukturorganisasi/app-strukturorganisasi', ['pageConfigs' => $pageConfigs]);
    }

    // Product Program Unggulan App
    public function jadwallayananApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'jadwallayanan-application',
        ];

        return view('/content/apps/jadwallayanan/app-jadwallayanan', ['pageConfigs' => $pageConfigs]);
    }












    // invoice list App
    public function invoice_list()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/apps/invoice/app-invoice-list', ['pageConfigs' => $pageConfigs]);
    }
    // User list App
    public function usermanagement_user()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/apps/usermanagement/app-usermanagement-user', ['pageConfigs' => $pageConfigs]);
    }

    // invoice preview App
    public function invoice_preview()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/apps/invoice/app-invoice-preview', ['pageConfigs' => $pageConfigs]);
    }

    // invoice edit App
    public function invoice_edit()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/apps/invoice/app-invoice-edit', ['pageConfigs' => $pageConfigs]);
    }

    // invoice edit App
    public function invoice_add()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/apps/invoice/app-invoice-add', ['pageConfigs' => $pageConfigs]);
    }
    // invoice edit App
    public function invoice_manageuser()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/apps/invoice/app-invoice-manageuser', ['pageConfigs' => $pageConfigs]);
    }

    // invoice print App
    public function invoice_print()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/apps/invoice/app-invoice-print', ['pageConfigs' => $pageConfigs]);
    }

    // User List Page
    public function user_list()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-list', ['pageConfigs' => $pageConfigs]);
    }

    // User Account Page
    public function user_view_account()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-account', ['pageConfigs' => $pageConfigs]);
    }

    // User Security Page
    public function user_view_security()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-security', ['pageConfigs' => $pageConfigs]);
    }

    // User Billing and Plans Page
    public function user_view_billing()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-billing', ['pageConfigs' => $pageConfigs]);
    }

    // User Notification Page
    public function user_view_notifications()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-notifications', ['pageConfigs' => $pageConfigs]);
    }

    // User Connections Page
    public function user_view_connections()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-connections', ['pageConfigs' => $pageConfigs]);
    }


    // Chat App
    public function chatApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'contentLayout' => "content-left-sidebar",
            'pageClass' => 'chat-application',
        ];

        return view('/content/apps/chat/app-chat', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    // Calender App
    public function calendarApp()
    {
        $pageConfigs = [
            'pageHeader' => false
        ];

        return view('/content/apps/calendar/app-calendar', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    // Email App
    public function emailApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'contentLayout' => "content-left-sidebar",
            'pageClass' => 'email-application',
        ];

        return view('/content/apps/email/app-email', ['pageConfigs' => $pageConfigs]);
    }
    // ToDo App
    public function todoApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'contentLayout' => "content-left-sidebar",
            'pageClass' => 'todo-application',
        ];

        return view('/content/apps/todo/app-todo', [
            'pageConfigs' => $pageConfigs
        ]);
    }
    // File manager App
    public function file_manager()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'contentLayout' => "content-left-sidebar",
            'pageClass' => 'file-manager-application',
        ];

        return view('/content/apps/fileManager/app-file-manager', ['pageConfigs' => $pageConfigs]);
    }

    // Access Roles App
    public function access_roles()
    {
        $pageConfigs = ['pageHeader' => false,];

        return view('/content/apps/rolesPermission/app-access-roles', ['pageConfigs' => $pageConfigs]);
    }

    // Access permission App
    public function access_permission()
    {
        $pageConfigs = ['pageHeader' => false,];

        return view('/content/apps/rolesPermission/app-access-permission', ['pageConfigs' => $pageConfigs]);
    }

    // Kanban App
    public function kanbanApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'kanban-application',
        ];

        return view('/content/apps/kanban/app-kanban', ['pageConfigs' => $pageConfigs]);
    }

    // Ecommerce Shop
    public function ecommerce_shop()
    {
        $pageConfigs = [
            'contentLayout' => "content-detached-left-sidebar",
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Shop"]
        ];

        return view('/content/apps/ecommerce/app-ecommerce-shop', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    // Ecommerce Details
    public function ecommerce_details()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['link' => "/app/ecommerce/shop", 'name' => "Shop"], ['name' => "Details"]
        ];

        return view('/content/apps/ecommerce/app-ecommerce-details', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    // Ecommerce Wish List
    public function ecommerce_wishlist()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Wish List"]
        ];

        return view('/content/apps/ecommerce/app-ecommerce-wishlist', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    // Ecommerce Checkout
    public function ecommerce_checkout()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/apps/ecommerce/app-ecommerce-checkout', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
